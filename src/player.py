class Player(object):
    '''
    Class to define the player functionalities
    '''
    def __init__(self,no_of_players):
        self.no_of_players = no_of_players
        self.player_points = {}
        self.player_moves = {}
        self.max_key_list=[]
        for player in range(1,self.no_of_players+1):
            self.player_points[player] = 0
            self.player_moves[player] = []

    def update_points(self, player_id, points):
        '''
        Method to update the player points and if the points goes negative it will be zero
        '''
        self.player_points[player_id] = 0 if self.player_points[player_id] + points < 0 \
            else self.player_points[player_id] + points

    def check_total_points(self):
        '''
        Method to check whether any of the player has crossed 5 points 
        '''
        for player in self.player_points:
            if self.player_points[player] >=5:
                return True
        return False

    def update_individual_moves(self, player_id, move):
        '''
        Method to append the moves to the history of moves list for the given player id
        '''
        points = self.player_points[player_id]
        self.player_moves[player_id].append({
                                             'move' : move,
                                             'points': points
                                             })

    def play_zero_strike_moves(self, player_id):
        '''
        Method to check whether the player has made three successive empty moves 
        '''
        moves = self.player_moves[player_id]
        counter = 0
        index = 0
        for i in moves[::-1]:
            if index > 3:
                break
            if i['move'] == 'empty':
                counter+=1
            index+=1
        if counter >= 3:
            self.update_points(player_id, -1)

    def play_foul_moves(self, player_id):
        '''
        Method to check whether the player has made foul three times in the history of moves
        '''
        moves = self.player_moves[player_id]
        counter = 0
        for i in moves:
            if i['move'] == 'defunct' or i['move'] == 'striker':
                counter+=1
        self.update_points(player_id, counter/3)

    def find_max_score_keys(self):
        '''
        Method to find the max scoring players list in descending order
        '''
        sorted_player_list = sorted(self.player_points.items(), key = lambda kv:(kv[1], kv[0]))
        self.max_key_list = sorted_player_list[::-1]

    def find_top_scorers(self):
        '''
        Method to find the match result
        '''   
        self.find_max_score_keys()
        if self.max_key_list[0][1]> self.max_key_list[1][1] and \
            self.max_key_list[0][1] - self.max_key_list[1][1] >= 3:
            return {
                    'result': 'Player '+str(self.max_key_list[0][0])
                   }
        else:
            return{
                   'result': 'Match Tied'
                   }             
            

    def compute_points_difference(self):
        '''
        Method to compute the points difference between the two players
        '''
        self.find_max_score_keys()
        return self.max_key_list[0][1] - self.max_key_list[1][1]