from constants import BLACK_COINS, RED_COINS, STRIKE_SCORE, MULTI_STRIKE_SCORE, RED_STRIKE_SCORE, \
   STRIKER_STRIKE_SCORE, DEFUNCT_SCORE, ZERO_STRIKE_SCORE

class CarromBoard(object):
    '''
    Class to define the carrom board functionalities
    '''
    def __init__(self):
        '''
        Constructor method to define the initial number of coins in the board
        '''
        self.black = BLACK_COINS
        self.red = RED_COINS

    def play_strike(self):
        '''
        When a player pockets a coin he/she wins a point
        '''
        if self.black == 0:
            return 0
        self.black -= 1
        return STRIKE_SCORE

    def play_multi_strike(self):
        '''
        When a player pockets more than one coin he/she wins 2 points. All, but 2
        coins, that were pocketed, get back on to the carrom-board
        '''
        if self.black == 0:
            return 0
        self.black = 7
        self.red = 1
        return MULTI_STRIKE_SCORE

    def play_red_strike(self):
        '''
        When a player pockets red coin he/she wins 3 points. If other coins are
        pocketed along with red coin in the same turn, other coins get back on to the
        carrom-board
        '''
        if self.red == 0:
            return 0
        self.red -= 1
        return RED_STRIKE_SCORE

    def play_striker_strike(self):
        '''
        When a player pockets the striker he/she loses a point
        '''
        return STRIKER_STRIKE_SCORE

    def play_defunct_coin(self):
        '''
        When a coin is thrown out of the carrom-board, due to a strike, the player
        loses 2 points, and the coin goes out of play
        '''
        if self.black == 0:
            return 0
        self.black -= 1
        return DEFUNCT_SCORE

    def play_zero_strike(self):
        '''
        When a player does not pocket a coin
        '''
        return ZERO_STRIKE_SCORE

    def check_coins(self):
        '''
        Check the number of coins on the board
        '''
        if self.black == 0 and self.red == 0:
            return True
        return False
