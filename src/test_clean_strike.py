from clean_strike import Clean_Strike_Game
import pytest


class Test_Clean_Strike_Game(object):
    
    @pytest.mark.test_win1
    def test_win1(self):
        exec_run = Clean_Strike_Game(2)
        options = [['Strike','Multistrike'],['Strike','Defunct'],['Redstrike','Defunct'],['Strike','Redstrike']]
        exec_run.play_game(options)
        assert exec_run.player_obj.player_points.pop(1) == 5
        assert exec_run.player_obj.player_points.pop(2) == 0

    @pytest.mark.test_win2
    def test_win2(self):
        exec_run = Clean_Strike_Game(2)
        options = [['Multistrike','Strike'],['Multistrike','Strike'],['Defunct','Strike'],['None','Redstrike']]
        exec_run.play_game(options)
        assert exec_run.player_obj.player_points.pop(1) == 2
        assert exec_run.player_obj.player_points.pop(2) == 6

    @pytest.mark.test_tie
    def test_tie(self):
        exec_run = Clean_Strike_Game(2)
        options = [['Strike','Multistrike'],['Strike','Multistrike']]
        exec_run.play_game(options)
        assert exec_run.player_obj.player_points.pop(1) == 2
        assert exec_run.player_obj.player_points.pop(2) == 4
        
    def test_more_than_2players(self):
        exec_run = Clean_Strike_Game(3)
        options = [['Strike','Multistrike','Strike'],['Strike','Defunct','Multistrike'],['Redstrike','Defunct','Defunct'],['Strike','Redstrike','None']]
        exec_run.play_game(options)
        assert exec_run.player_obj.player_points.pop(1) == 5
        assert exec_run.player_obj.player_points.pop(2) == 0
        assert exec_run.player_obj.player_points.pop(3) == 1