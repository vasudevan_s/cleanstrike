from carrom_board import CarromBoard
from player import Player

class Clean_Strike_Game(object):
    '''
    Class where the actual game begins
    '''
    def __init__(self, no_of_players):
        self.board_obj = CarromBoard()
        self.player_obj = Player(no_of_players)

    def check_game_conditions(self):
        '''
        Check the game ending conditions which are,
            1. player has scored more than 5 points
            2. difference in score  between the two players are more than three points
            3. if the number of black coins and red coins becomes zero
        '''
        if(self.player_obj.check_total_points() and \
           self.player_obj.compute_points_difference() >= 3) or (self.board_obj.check_coins()):
            scorecard = self.player_obj.find_top_scorers()
            print "Final result ::"+scorecard['result']
            return True
        return False

    def play_game(self, options):
        '''
        Get the options as input and plays the game
        '''
        if self.player_obj.no_of_players < 2:
            print "Match can't be played with one player"
            return

        for play in options:
            player_id = 1
            if len(play) != self.player_obj.no_of_players:
                print "Provide actions for each player"
                return

            for action in play:
                if action == 'Strike':
                    points = self.board_obj.play_strike()
                    self.player_obj.update_points(player_id, points)
                    self.player_obj.update_individual_moves(player_id, action)
                    player_id += 1
                elif action == 'Multistrike':
                    points = self.board_obj.play_multi_strike()
                    self.player_obj.update_points(player_id, points)
                    self.player_obj.update_individual_moves(player_id, action)
                    player_id += 1
                elif action == 'Redstrike':
                    points = self.board_obj.play_red_strike()
                    self.player_obj.update_points(player_id, points)
                    self.player_obj.update_individual_moves(player_id, action)
                    player_id += 1
                elif action == 'Strikerstrike':
                    points = self.board_obj.play_striker_strike()
                    self.player_obj.update_points(player_id, points)
                    self.player_obj.update_individual_moves(player_id, action)
                    self.player_obj.play_foul_moves(player_id)
                    player_id += 1
                elif action == 'Defunct':
                    points = self.board_obj.play_defunct_coin()
                    self.player_obj.update_points(player_id, points)
                    self.player_obj.update_individual_moves(player_id, action)
                    self.player_obj.play_foul_moves(player_id)
                    player_id += 1
                elif action == 'None':
                    points = self.board_obj.play_zero_strike()
                    self.player_obj.update_points(player_id, points)
                    self.player_obj.update_individual_moves(player_id, action)
                    self.player_obj.play_zero_strike_moves(player_id)
                    player_id += 1
                else:
                    print "Please check the move :: "+str(action)
                    return
            if self.check_game_conditions():
                return
        print "Final result :: Match tied"
        return

if __name__ == "__main__":
    no_of_players = 2
    options = [['Strike', 'Multistrike'], ['Strike', 'Multistrike'], ['Defunct', 'Defunct'], \
               ['Redstrike','Redstrike']]
    if no_of_players > 1:
        exec_run = Clean_Strike_Game(no_of_players)
        exec_run.play_game(options)
    else:
        print "Match can't be played with one player"
