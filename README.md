To run the python main script directly,
1. Go to the path containing the clean_strike.py
2. Change the number of players and actions respectively for each player
3. Run the command,
	>python clean_strike.py 

To run the test file,

1. Run the get-pip.py file from the resources folder in a command shell
2. Run the command
	> python -m pip install pytest
3. Go to the test_clean_strike.py file path and run the below command
	> python -m py.test -vvv test_clean_strike.py
